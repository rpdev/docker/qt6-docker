# Minimal docker container to build project

FROM ubuntu:20.04

ARG QT_VERSION=6.8.1
ARG QT_CI_LOGIN
ARG QT_CI_PASSWORD
ARG QT_INSTALLER_URL=http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run


ENV DEBIAN_FRONTEND=noninteractive \
    QT_INSTALL_ROOT=/opt/Qt \
    QT_PATH=/opt/Qt/${QT_VERSION}/gcc_64/bin

RUN dpkg --add-architecture i386 && apt update && apt full-upgrade -y && apt install -y --no-install-recommends \
    appstream \
    build-essential \
    ca-certificates \
    ccache \
    clazy \
    cmake \
    curl \
    desktop-file-utils \
    file \
    git \
    git-lfs \
    libc6 \
    libdbus-1-3 \
    libfontconfig1 \
    libfuse2 \
    libgl1-mesa-dev \
    libgles2-mesa-dev \
    libice6 \
    libmysqlclient21 \
    libncurses5 \
    libodbc1 \
    libpq5 \
    libsecret-1-dev \
    libsm6 \
    libssl-dev \
    libstdc++6 \
    libwayland-dev \
    libwayland-egl1-mesa \
    libwayland-server0 \
    libx11-xcb1 \
    libxcb-glx0 \
    libxcb-icccm4 \
    libxcb-image0 \
    libxcb-keysyms1 \
    libxcb-randr0 \
    libxcb-render-util0 \
    libxcb-shape0 \
    libxcb-sync1 \
    libxcb-xfixes0 \
    libxcb-xinerama0 \
    libxcb-cursor0 \
    libxcomposite1 \
    libxext6 \
    libxkbcommon-dev \
    libxkbcommon-x11-0 \
    libxrender1 \
    libz1 \
    locales \
    make \
    ninja-build \
    openssh-client \
    patchelf \
    pkg-config \
    python3 \
    python3-pip \
    python3-venv \
    unzip \
    && apt-get -qq clean \
    && rm -rf /var/lib/apt/lists/* \
    && git-lfs install --system

COPY install-qt.sh /tmp/qt/

# Download & unpack Qt toolchains & clean
RUN bash /tmp/qt/install-qt.sh

# Reconfigure locale
RUN locale-gen en_US.UTF-8 && dpkg-reconfigure locales
